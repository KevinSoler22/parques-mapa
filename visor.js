function init(){
    var mymap = L.map('myMap',{center:[4.675897,-74.094417],zoom:16});
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoia2V2aW5zb2xlciIsImEiOiJja2N3ang4YmQwOWtmMnpzemd0bHlkODZzIn0.F08kJOFz-HizLt33S6k-Sg'
    }).addTo(mymap);

    var parkIcon = L.icon({
        iconUrl: 'arbol.jpg',
        shadowUrl: 'arbol.jpg',
    
        iconSize:     [38, 95], 
        shadowSize:   [50, 64], 
        iconAnchor:   [22, 94], 
        shadowAnchor: [4, 62],  
    });

    var marker=L.marker([4.658916,-74.093430]).addTo(mymap);
    var marker1=L.marker([4.597675,-74.081353]).addTo(mymap);
    var marker2=L.marker([4.663551,-74.087005]).addTo(mymap);
    var marker3=L.marker([4.671080,-74.097620]).addTo(mymap);
    var markerPrueba=L.marker([4.676172,-74.351751],{icon:parkIcon}).addTo(mymap);

    marker.bindPopup("Parque Simon Bolivar").openPopup();
    marker1.bindPopup("Parque Tercer Milenio").openPopup();
    marker2.bindPopup("Parque El Salitre").openPopup();
    marker3.bindPopup("Donde se galgea en el barrio!!").openPopup(); 
    
    var circle = L.circle([4.699764, -74.146049], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 500
    }).addTo(mymap);

    var polygon = L.polygon([
        [4.639387,-74.222490],
        [4.724931,-74.159319],
        [4.835096,-74.082414],
        [4.815938,-74.007570],
        [4.671553,-74.003450],
        [4.596268,-73.993150],
        [4.475797,-74.079668],
        [4.473059,-74.176485],
        [4.594215,-74.184724    ]
        
    ]).addTo(mymap);
    
}
